package ch.rs.jddcd.data.device;

import ch.rs.jddcd.data.device.slave.SlaveRank;

public class DiscoveredDevice {

    private String IP;
    private int definedPort;
    private SlaveRank slaveRank;

    public void DiscoveredDevice(String IP, int definedPort){
        this.IP = IP;
        this.definedPort = definedPort;
    };

    public void setSlaveRank(int slaveRank){
        this.slaveRank = new SlaveRank(slaveRank);
    }



}
