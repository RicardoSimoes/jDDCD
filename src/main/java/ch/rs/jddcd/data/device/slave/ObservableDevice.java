package ch.rs.jddcd.data.device.slave;

public class ObservableDevice {

    private String IP;
    private int observingPort;

    public ObservableDevice(String IP, int observingPort){
        this.IP = IP;
        this.observingPort = observingPort;
    }


    public String getIP(){
        return IP;
    }

    public int getObservationPort(){
        return observingPort;
    }

}
