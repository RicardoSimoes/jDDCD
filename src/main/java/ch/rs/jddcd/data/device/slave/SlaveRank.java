package ch.rs.jddcd.data.device.slave;

public class SlaveRank {

    /**
     * slaveRank == 0 would mean this device is the master.
     * If the next slave above goes missing, this device gets the new rank
     * slaveRank--;
     */
    private int slaveRank;

    public SlaveRank(int slaveRank){
        this.slaveRank = slaveRank;
    }


    public void increaseRank(){
        slaveRank--;
    }

    public int getSlaveRank(){
        return slaveRank;
    }
}
