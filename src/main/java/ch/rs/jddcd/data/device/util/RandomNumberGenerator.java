package ch.rs.jddcd.data.device.util;


import java.util.concurrent.ThreadLocalRandom;

public class RandomNumberGenerator {

    private static int MIN = 0;
    private static int MAX = 400;

    public static int generateRandomRank(){
        return ThreadLocalRandom.current().nextInt(MIN, MAX + 1);
    }
}
